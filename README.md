## Project Setup
1. Install node 12+ version
2. Generate `.env` from `.env.example`.

### Install dependencies
```bash
$ npm install
```

### Start a project
```bash
$ npm run start
```

### Start a Unit Test
```bash
$ npm run test
```