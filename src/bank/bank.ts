import Transaction from "../transaction/transaction";
import { IOptions, ITransaction } from "../interfaces";
import { calculateCommission } from "./bank.services";
import { getMonday, getSunday, inRange } from "./bank.utils";
import { CASH_IN, CASH_OUT, JURIDICAL_USER, NATURAL_USER } from "../constants";

export default class Bank {
  private readonly transactions: Transaction[];
  private options: IOptions;

  constructor(options: IOptions) {
    this.transactions = [];
    this.options = options;
  }

  getTransactions(): ITransaction[] {
    return this.transactions;
  }

  private calculateFee(transaction: Omit<ITransaction, "fee">): number {
    try {
      if (transaction.type === CASH_IN) {
        return calculateCommission(
          transaction.operation.amount,
          this.options.cashIn
        );
      }

      if (
        transaction.type === CASH_OUT &&
        transaction.user_type === JURIDICAL_USER
      ) {
        return calculateCommission(
          transaction.operation.amount,
          this.options.cashOutJuridical
        );
      }

      if (
        transaction.type === CASH_OUT &&
        transaction.user_type === NATURAL_USER
      ) {
        const startWeek = getMonday(transaction.date);
        const endWeek = getSunday(transaction.date);
        const filtered = this.transactions.filter(
          (item) =>
            inRange(item.date, startWeek, endWeek) &&
            item.type === CASH_OUT &&
            item.user_type === NATURAL_USER &&
            item.user_id === transaction.user_id
        );

        const sum = filtered.reduce(
          (res, value) => res + value.operation.amount,
          0
        );

        return calculateCommission(
          transaction.operation.amount,
          this.options.cashOutNatural,
          sum
        );
      }

      return 0;
    } catch (error) {
      console.error(error);
    }
  }

  addTransaction(transaction: Omit<ITransaction, "fee">): ITransaction {
    try {
      const newTransaction: Transaction = new Transaction({
        ...transaction,
        fee: this.calculateFee(transaction),
      });
      this.transactions.push(newTransaction);
      return newTransaction;
    } catch (error) {
      console.error(error);
    }
  }
}
