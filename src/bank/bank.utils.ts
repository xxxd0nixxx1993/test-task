import {
  isMonday,
  previousMonday,
  isSunday,
  nextSunday,
  isWithinInterval,
} from "date-fns";

export function getMonday(d: Date | string): Date {
  const date = new Date(d);

  if (isMonday(date)) {
    return date;
  }

  return previousMonday(date);
}

export function getSunday(d: Date | string): Date {
  const date = new Date(d);

  if (isSunday(date)) {
    return date;
  }

  return nextSunday(date);
}

export function inRange(
  d: Date | string,
  startDate: Date,
  endDate: Date
): boolean {
  const date = new Date(d);

  return isWithinInterval(date, {
    start: startDate,
    end: endDate,
  });
}

export function roundFee(v: number): number {
  return Math.round(v * 1e2) / 1e2;
}
