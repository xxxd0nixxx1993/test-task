import { roundFee } from "./bank.utils";
import { IOption, ITransaction } from "../interfaces";

export function calculateCommission(
  amount: ITransaction["operation"]["amount"],
  option: IOption,
  sum?: number
) {
  let fee = roundFee((amount / 100) * option.percents);

  if (option.hasOwnProperty("week_limit")) {
    // How many free of charge user have on this week
    const freeLost = Math.max(0, option.week_limit.amount - sum);
    // How user charge over free of charge week limit
    const feeAmount = Math.max(0, amount - freeLost);
    fee = roundFee((feeAmount / 100) * option.percents);
  }

  if (option.hasOwnProperty("min") && fee < option.min.amount) {
    return option.min.amount;
  }

  if (option.hasOwnProperty("max") && fee > option.max.amount) {
    return option.max.amount;
  }

  return fee;
}
