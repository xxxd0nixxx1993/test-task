import Bank from "./bank";
import { IOption, IOptions, ITransaction } from "../interfaces";
import { getMonday, getSunday, inRange, roundFee } from "./bank.utils";
import { calculateCommission } from "./bank.services";
import { CASH_IN, CASH_OUT, JURIDICAL_USER, NATURAL_USER } from "../constants";

const testData: Omit<ITransaction, "fee">[] = [
  {
    date: new Date("2016-01-05"),
    user_id: 1,
    user_type: "natural",
    type: "cash_in",
    operation: { amount: 200.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-06"),
    user_id: 2,
    user_type: "juridical",
    type: "cash_out",
    operation: { amount: 300.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-06"),
    user_id: 1,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 30000, currency: "EUR" },
  },
  {
    date: new Date("2016-01-07"),
    user_id: 1,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 1000.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-07"),
    user_id: 1,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 100.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-10"),
    user_id: 1,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 100.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-10"),
    user_id: 2,
    user_type: "juridical",
    type: "cash_in",
    operation: { amount: 1000000.0, currency: "EUR" },
  },
  {
    date: new Date("2016-01-10"),
    user_id: 3,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 1000.0, currency: "EUR" },
  },
  {
    date: new Date("2016-02-15"),
    user_id: 1,
    user_type: "natural",
    type: "cash_out",
    operation: { amount: 300.0, currency: "EUR" },
  },
];

const testOption: IOptions = {
  cashIn: {
    percents: 0.03,
    max: { amount: 5, currency: "EUR" },
  },
  cashOutNatural: {
    percents: 0.3,
    week_limit: { amount: 1000, currency: "EUR" },
  },
  cashOutJuridical: {
    percents: 0.3,
    min: { amount: 0.5, currency: "EUR" },
  },
};

describe("Test bank class", () => {
  it("should return array equal test result data", () => {
    const bank = new Bank(testOption);
    testData.forEach((item) => bank.addTransaction(item));
    const result = bank.getTransactions().map(({ fee }) => fee);

    expect(result).toEqual([0.06, 0.9, 87.0, 3.0, 0.3, 0.3, 5.0, 0.0, 0.0]);
  });
});

describe("Test roundFee utils", () => {
  it("should return - 2.63", () => {
    expect(roundFee(2.631)).toEqual(2.63);
  });

  it("should return - 3.80", () => {
    expect(roundFee(3.798)).toEqual(3.8);
  });
});

describe("Test getMonday utils", () => {
  it("should return - 2022-02-14", () => {
    const date = new Date("2022-02-15");
    const monday = new Date("2022-02-14");

    expect(getMonday(date)).toEqual(monday);
  });
});

describe("Test getSunday utils", () => {
  it("should return - 2022-02-20", () => {
    const date = new Date("2022-02-15");
    const sunday = new Date("2022-02-20");

    expect(getSunday(date)).toEqual(sunday);
  });
});

describe("Test inRange utils", () => {
  it("should return - true", () => {
    const date = new Date("2022-02-15");
    const start = new Date("2022-02-14");
    const end = new Date("2022-02-20");

    expect(inRange(date, start, end)).toEqual(true);
  });

  it("should return - false", () => {
    const date = new Date("2022-02-12");
    const start = new Date("2022-02-14");
    const end = new Date("2022-02-20");

    expect(inRange(date, start, end)).toEqual(false);
  });
});

describe(`Test calculateCommission type=${CASH_IN} services`, () => {
  it("should return - 0.06", () => {
    expect(calculateCommission(200.0, testOption.cashIn)).toEqual(0.06);
  });

  it("should return - 5", () => {
    expect(calculateCommission(1000000.0, testOption.cashIn)).toEqual(5);
  });
});

describe(`Test calculateCommission type=${CASH_OUT} user_type=${NATURAL_USER} services`, () => {
  it("should return - 87", () => {
    expect(calculateCommission(30000, testOption.cashOutNatural, 0)).toEqual(
      87
    );
  });

  it("should return - 3", () => {
    expect(calculateCommission(1000, testOption.cashOutNatural, 30000)).toEqual(
      3
    );
  });
});

describe(`Test calculateCommission type=${CASH_OUT} user_type=${JURIDICAL_USER} services`, () => {
  it("should return - 0.9", () => {
    expect(calculateCommission(300, testOption.cashOutJuridical)).toEqual(0.9);
  });

  it("should return - 0.5", () => {
    expect(calculateCommission(50, testOption.cashOutJuridical)).toEqual(0.5);
  });
});

describe(`Test calculateCommission services with custom options`, () => {
  const customOptionLimitMin: IOption = {
    percents: 0.3,
    week_limit: { amount: 1000, currency: "EUR" },
    min: { amount: 0.5, currency: "EUR" },
  };

  it("should return - 0.5", () => {
    expect(calculateCommission(1000, customOptionLimitMin, 0)).toEqual(0.5);
  });

  const customOptionLimitMax: IOption = {
    percents: 0.3,
    week_limit: { amount: 1000, currency: "EUR" },
    max: { amount: 5, currency: "EUR" },
  };

  it("should return - 5", () => {
    expect(calculateCommission(20000, customOptionLimitMax, 1000)).toEqual(5);
  });

  const customOptionMinMax: IOption = {
    percents: 0.3,
    min: { amount: 0.7, currency: "EUR" },
    max: { amount: 8, currency: "EUR" },
  };

  it("should return - 0.7", () => {
    expect(calculateCommission(50, customOptionMinMax)).toEqual(0.7);
  });

  it("should return - 8", () => {
    expect(calculateCommission(50000, customOptionMinMax)).toEqual(8);
  });
});
