import { createReadStream } from "fs";
import { parse } from "jsonstream";
require("dotenv").config();

import Bank from "./bank/bank";
import { ITransaction } from "./interfaces";
import { api } from "./api";

const stream = createReadStream("input.json", { encoding: "utf-8" });

(async () => {
  const options = await Promise.all([
    api({
      method: "GET",
      url: "/cash-in",
    }),
    api({
      method: "GET",
      url: "/cash-out-natural",
    }),
    api({
      method: "GET",
      url: "/cash-out-juridical",
    }),
  ]).then(([cashIn, cashOutNatural, cashOutJuridical]) => ({
    cashIn,
    cashOutNatural,
    cashOutJuridical,
  }));

  const bank = new Bank(options);

  stream
    .pipe(parse("*"))
    .on("data", (data: ITransaction) => {
      const transaction = bank.addTransaction(data);
      console.log(transaction.fee);
    })
    .on("error", console.error);
})();
