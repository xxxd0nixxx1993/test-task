import Operation from "../operations/operation";
import { ITransaction } from "../interfaces";

export default class Transaction implements ITransaction {
  operation;
  type;
  user_type;
  user_id;
  date;
  fee;

  constructor(props: ITransaction) {
    this.date = props.date;
    this.user_id = props.user_id;
    this.user_type = props.user_type;
    this.type = props.type;
    this.operation = new Operation(props.operation);
    this.fee = props.fee;
  }
}
