import { IOperation } from "../interfaces";

export default class Operation implements IOperation {
  amount;
  currency;

  constructor(props: IOperation) {
    this.amount = props.amount;
    this.currency = props.currency;
  }
}
